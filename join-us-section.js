export class Section {
  constructor(title, submitText) {
    this.title = title;
    this.submitText = submitText;
    this.element = this.create();
  }

  create() {
    const joinUp = document.getElementById("joinUp");
    joinUp.classList.add("joinUp");

    const title = document.createElement("h1");
    title.innerHTML = "Join Our Program";
    joinUp.appendChild(title);

    const paragraph = document.createElement("p");
    paragraph.innerHTML =
      "Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna aliqua.";
    joinUp.appendChild(paragraph);

    const sectionForm = document.createElement("form");
    sectionForm.classList.add("submit");

    const textInput = document.createElement("input");
    textInput.type = "text";
    textInput.placeholder = "Email";
    const textInputClass = ["submit", "input"];
    textInput.classList.add(...textInputClass);

    const submitBtn = document.createElement("button");
    const submitBtnClass = ["submit", "button"];
    submitBtn.innerHTML = "Subscribe";
    submitBtn.type = "submit";
    submitBtn.id = "subscribe";
    submitBtn.classList.add(...submitBtnClass);
    sectionForm.append(textInput, submitBtn);

    // Add event listener on form section
    sectionForm.addEventListener("submit", (e) => {
      e.preventDefault();
      const enteredValue = e.target[0].value;
      console.log(`Entered value is: ${enteredValue}`);
      e.target[0].value = "";
    });

    joinUp.append(sectionForm);
  }
}

export class SectionCreator {
  create(type) {
    switch (type) {
      case "standard":
        return new Section("Join Our Program", "Subscribe");
      case "advanced":
        return new Section(
          "Join Our Advanced Program",
          "Subscribe to Advanced Program"
        );
      default:
        return null;
    }
  }
}
