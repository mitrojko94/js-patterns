import { describe, it } from "mocha";
import { expect } from "chai";
import { jest, beforeEach, afterEach } from "jest";

import {
  validateAsync,
  validateWithThrow,
  validateWithLog,
} from "./email-validator";

describe("validateAsync", () => {
  it("Valid email should resolve to true", async () => {
    const result = await validateAsync("test@example.com");
    expect(result).toBe(true);
  });

  it("Invalid email should resolve to false", async () => {
    const result = await validateAsync("test@example");
    expect(result).toBe(false);
  });
});

describe("validateWithThrow", () => {
  it("Valid email should return true", () => {
    expect(validateWithThrow("test@example.com")).toBe(true);
  });

  it("Invalid email should throw error", () => {
    expect(() => {
      validateWithThrow("test@example");
    }).toThrow("Invalid email provided");
  });
});

describe("validateWithLog", () => {
  let originalConsoleLog;

  beforeEach(() => {
    originalConsoleLog = console.log;
    console.log = jest.fn();
  });

  afterEach(() => {
    console.log = originalConsoleLog;
  });

  it("Valid email should log correct message", () => {
    validateWithLog("test@example.com");
    expect(console.log).toHaveBeenCalledWith(
      "Email test@example.com is valid: true"
    );
  });

  it("Invalid email should log correct message", () => {
    validateWithLog("test@example");
    expect(console.log).toHaveBeenCalledWith(
      "Email test@example is valid: false"
    );
  });
});
