import { validate } from "./email-validator";

export class Section {
  constructor(title, submitText) {
    this.title = title;
    this.submitText = submitText;
    this.element = this.create();
  }

  create() {
    const joinUp = document.getElementById("joinUp");
    joinUp.classList.add("joinUp");

    const title = document.createElement("h1");
    title.innerHTML = "Join Our Program";
    joinUp.appendChild(title);

    const paragraph = document.createElement("p");
    paragraph.innerHTML =
      "Sed do eiusmod tempor incididunt <br /> ut labore et dolore magna aliqua.";
    joinUp.appendChild(paragraph);

    const sectionForm = document.createElement("form");
    sectionForm.classList.add("submit");

    const textInput = document.createElement("input");
    textInput.type = "text";
    textInput.placeholder = "Email";
    textInput.value = "";
    textInput.id = "email";
    const textInputClass = ["submit", "input"];
    textInput.classList.add(...textInputClass);

    const submitBtn = document.createElement("button");
    const submitBtnClass = ["submit", "button"];
    submitBtn.innerHTML = "Subscribe";
    submitBtn.type = "submit";
    submitBtn.id = "subscribe";
    submitBtn.classList.add(...submitBtnClass);
    sectionForm.append(textInput, submitBtn);

    joinUp.append(sectionForm);

    // FORM
    const emailInput = document.getElementById("email");
    if (localStorage.getItem("subscriptionEmail")) {
      emailInput.value = localStorage.getItem("subscriptionEmail");
    }

    emailInput.addEventListener("input", function () {
      localStorage.setItem("subscriptionEmail", emailInput.value);
    });

    sectionForm.addEventListener("submit", function (event) {
      event.preventDefault();
      const email = emailInput.value.trim();
      console.log(email);

      // If email is valid, hide email input and change button text
      if (localStorage.getItem("subscribed")) {
        localStorage.removeItem("subscriptionEmail");
        localStorage.removeItem("subscribed");
        submitBtn.textContent = "Subscribe";
        emailInput.style.display = "block";
      } else {
        submitBtn.textContent = "Unsubscribe";
        submitBtn.style.margin = "0 auto";
        emailInput.style.display = "none";
        localStorage.setItem("subscribed", true);
      }

      // AJAX
      const fetchCommunityData = async () => {
        try {
          const response = await fetch("http://localhost:8000/community");
          const data = await response.json();
          console.log(data);
        } catch (error) {
          console.error("Error fetching community data:", error);
        }
      };
      console.log(fetchCommunityData);

      // Subscribe and Unsubscribe
      const isValid = validate(email);

      if (isValid && submitBtn.innerHTML !== "Unsubscribe") {
        const bodyData = {
          email: email,
        };

        submitBtn.disabled = true;
        submitBtn.style.opacity = 0.5;

        fetch("http://localhost:8000/subscribe", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(bodyData),
        })
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            submitBtn.disabled = false;
            submitBtn.style.opacity = 1;
            if (data.error) {
              throw new Error(data.error);
            } else {
              textInput.style.display = "none";
              submitBtn.innerHTML = "Unsubscribe";
            }
          })
          .catch((error) => {
            window.alert(error);
          });
      } else if (submitBtn.innerHTML === "Unsubscribe") {
        submitBtn.disabled = true;
        submitBtn.style.opacity = 0.5;

        fetch("http://localhost:8000/unsubscribe", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
        })
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            submitBtn.disabled = false;
            submitBtn.style.opacity = 1;
            if (data.success) {
              textInput.style.display = "inline";
              textInput.value = "";
              submitBtn.innerHTML = "Subscribe";
            }
          });
      } else {
        window.alert("Please enter valid email.");
      }
    });
  }
}

export class SectionCreator {
  create(type) {
    switch (type) {
      case "standard":
        return new Section("Join Our Program", "Subscribe");
      case "advanced":
        return new Section(
          "Join Our Advanced Program",
          "Subscribe to Advanced Program"
        );
      default:
        return null;
    }
  }
}
