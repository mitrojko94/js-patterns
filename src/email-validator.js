const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

export function validate(email) {
  const newEmail = email.split("@");
  const newEmail1 = newEmail[1];

  if (VALID_EMAIL_ENDINGS.includes(`${newEmail1}`)) {
    return true;
  } else {
    return false;
  }
}

// Function to validate email asynchronously
export async function validateAsync(email) {
  return new Promise((resolve) => {
    // Example of async validation
    setTimeout(() => {
      resolve(email.endsWith(".com"));
    }, 1000);
  });
}

// Function to validate email and throw error if invalid
export function validateWithThrow(email) {
  if (!email.endsWith(".com")) {
    throw new Error("Invalid email provided");
  }
  return true;
}

// Function to validate email and log result
export function validateWithLog(email) {
  const isValid = email.endsWith(".com");
  console.log(`Email ${email} is valid: ${isValid}`);
  return isValid;
}
