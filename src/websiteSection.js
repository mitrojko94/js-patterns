class WebsiteSection extends HTMLElement {
  constructor() {
    super();
    const shadow = this.attachShadow({ mode: "open" });

    const title = this.getAttribute("title");
    const description = this.getAttribute("description");

    const section = document.createElement("section");
    section.innerHTML = `
        <h2>${title}</h2>
        <p>${description}</p>
        <slot></slot>
      `;

    shadow.appendChild(section);
  }
}

customElements.define("website-section", WebsiteSection);
