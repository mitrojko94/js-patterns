module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: "eslint:recommended",
  overrides: [
    {
      env: {
        node: true,
      },
      files: [".eslintrc.{js,cjs}"],
      parserOptions: {
        sourceType: "script",
      },
    },
  ],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    eqeqeue: "off",
    curly: "error",
    quotes: ["error", "double"],
    camelcase: "error",
    "no-class-assign": "error",
    "no-const-assign": "error",
    "arrow-body-style": ["error", "always"],
    "capitalized-comments": "error",
    "dot-notation": "error",
    "no-array-constructor": "error",
  },
};
